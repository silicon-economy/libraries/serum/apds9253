# apds9253

This crate implements the driver for the APDS9253 RGB, ambient and IR light sensor from Broadcom. The sensor can be initialized in RGB mode, where all color channels and the infrared channel is available or in ALS mode, where only the ambient light and the infrared light channel is available.

## Resources

[Broadcom APDS9253](https://www.broadcom.com/products/optical-sensors/ambient-light-photo-sensors/apds-9253-001)

[APDS9253 Datasheet](https://docs.broadcom.com/doc/APDS-9253-001-DS)

## Features

- Up to 20-bit resolution
- I2C compatible interface with dedicated interrupt pin
- Individual channels for red (R), green (G), blue (B) and infrared (IR)
- Approximate human eye response with green channel
- ALS and RGB sensing with integrated IR-blocking filter
- Low power consumption

## Examples

The APDS has to be configured in RGB or ALS mode.

ALS mode:
```rust, no_run
## use embedded_hal_mock::*;
## let expectations = [];
## let mut i2c = i2c::Mock::new(&expectations);
##
use apds9253::*;

let mut sensor = Apds9253::new_als(i2c);
sensor.init().unwrap();

let ambient = sensor.read_ambient_light();
let infrared = sensor.read_infrared();

log::info!("Ambient: {ambient:?}, Infrared: {infrared:?}");

```rust

RGB mode:
```rust, no_run
#
use apds9253::*;

let mut sensor = Apds9253::new_rgb(i2c);
sensor.init().unwrap();

let red = sensor.read_red_channel();
let green = sensor.read_green_channel();
let blue = sensor.read_blue_channel();
let infrared = sensor.read_infrared();

log::info!("Red: {red:?}, Green: {green:?}, Blue: {blue:?}, Infrared: {infrared:?}");

```

## License

Open Logistics Foundation License\
Version 1.3, January 2023

See the LICENSE file in the top-level directory.

## Contact Information

Fraunhofer IML Embedded Rust Group - <embedded-rust@iml.fraunhofer.de>
